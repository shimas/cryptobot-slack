#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import time
import re
import urllib2
import json
import threading
import paramiko
import sqlite3
from datetime import datetime
from slackclient import SlackClient

# instantiate Slack client
slack_client = SlackClient(os.environ.get('SLACK_BOT_TOKEN'))
print(os.environ.get('SLACK_BOT_TOKEN'))

starterbot_id = None

# constants
RTM_READ_DELAY = 1 # 1 second delay between reading from RTM
BTC_MON_DELAY = 90
MENTION_REGEX = "^(<@.[^ ]+)(.*)"
my_channel="alerts"
sshHost=""
sshUsr=""

# commands
CMD = "get"

coinDesk_btc = "https://api.coindesk.com/v1/bpi/currentprice.json"
hdr = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
       'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
       'Accept-Charset': 'ISO-8859-1,utf-8;q=0.7,*;q=0.3',
       'Accept-Encoding': 'none',
       'Accept-Language': 'en-US,en;q=0.8',
       'Connection': 'keep-alive'}


class AddDaemon(object):
    def __init__(self):
        self.start_price=get_btc_prices()[1]

    def add(self):
      start_price=get_btc_prices()[1]
      start_price_E=get_btc_prices()[2]
      while True:
        cur_price=get_btc_prices()[1]
        cur_price_E=get_btc_prices()[2]
        diff_price = start_price - cur_price
        print("diff: " + str(diff_price))
        insert2db(diff_price,"diff")

        # btc goes down
        if diff_price > 100:
          printMessage("BTC price is going DOWN: start price:" + str(start_price) + "$ / " + str(start_price_E) + "€ | current price: " + str(cur_price) + "$ / " + str(cur_price_E) + "€", my_channel)
          start_price = cur_price
          start_price_E = cur_price_E
          insert2db(cur_price,"down")

        # btc goes up
        if diff_price < -75:
          printMessage("BTC price is going UP: start price:" + str(start_price) + "$ / " + str(start_price_E) + "€ | current price: " + str(cur_price) + "$ / " + str(cur_price_E) + "€", my_channel)
          start_price = cur_price
          start_price_E = cur_price_E
          insert2db(cur_price,"up")


        time.sleep(BTC_MON_DELAY)


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == "<@" + starterbot_id + ">":
              return message, event["channel"]
    return None, None

def insert2db(price,action):
  conn = sqlite3.connect('btcdb.sqlite')
  c = conn.cursor()
  data = datetime.now().strftime('%Y%m%d %H:%M:%S',)
  c.execute("INSERT INTO prices (price,action,date) VALUES (?, ?, ?)", (price,action,data))
  conn.commit()
  conn.close()

def get_btc_prices():
  req = urllib2.Request(coinDesk_btc,headers=hdr)
  opener = urllib2.build_opener()
  f = opener.open(req)
  jsonOut = json.loads(f.read())
  usd_price = int(round(jsonOut['bpi']['USD']['rate_float']))
  eur_price = int(round(jsonOut['bpi']['EUR']['rate_float']))
  response = "EUR: " + str(eur_price) + " / USD: " + str(usd_price)
  return response, usd_price, eur_price

def connectSSH(host, user, cmd):
  ssh = paramiko.SSHClient()
  ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
  ssh_stdin = ssh_stdout = ssh_stderr = None
  
  try:
    ssh.connect(host, username=user)
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(cmd)
  except Exception as e:
    sys.stderr.write("SSH connection error: {0}".format(e))

  if ssh_stdout:
    stdMsg = ssh_stdout.read()
    response = stdMsg
    return response
    
  if ssh_stderr:
    stdMsg = sys.stderr.write(ssh_stderr.read())


def parse_direct_mention(message_text):
  """
      Finds a direct mention (a mention that is at the beginning) in message text
      and returns the user ID which was mentioned. If there is no direct mention, returns None
  """
  matches = re.search(MENTION_REGEX, message_text)
  if matches:
    cmds = re.sub('<http?.[^|]+.','',matches.group(2).strip()).replace('>','')
  # the first group contains the username, the second group contains the remaining message
  return (matches.group(1), cmds) if matches else (None, None)

def handle_command(command, channel):
  """
      Executes bot command if the command is known
  """
  # Default response is help text for the user
  default_response = "Not sure what you mean. Try *{}*.".format(CMD)

  # Finds and executes the given command, filling in response
  response = None
  # This is where you start to implement more commands!
  if command == "get":
    response="BTC price: " + str(get_btc_prices()[1]) + "$ / " + str(get_btc_prices()[2]) + "€"
  elif command == "get eur":
    response="BTC price: " + str(get_btc_prices()[2]) + "€"
  elif command == "get usd":
    response="BTC price: " + str(get_btc_prices()[1]) + "$"
  elif command.split(' ')[0] == "rdns":
    print(command.split(' '))
    response = connectSSH(sshHost, sshUsr, "cd /root/ptr-zone-editor && python3 reverse.py %s %s" % (command.split(' ')[1], command.split(' ')[2]))

    
  # Sends the response back to the channel
  slack_client.api_call(
      "chat.postMessage",
      channel=channel,
      text=response or default_response
  )


def printMessage(message, channel):
  
  slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=message
  )

if __name__ == "__main__":
  if slack_client.rtm_connect(with_team_state=False):
    print("Starter Bot connected and running!")
    # Read bot's user ID by calling Web API method `auth.test`
    starterbot_id = slack_client.api_call("auth.test")["user_id"]
    print(starterbot_id)

    a = AddDaemon()
    t2 = threading.Thread(target=a.add)
    t2.setDaemon(True)
    t2.start()

    while True:
      command, channel = parse_bot_commands(slack_client.rtm_read())
      if command:
        handle_command(command, channel)
      
      time.sleep(RTM_READ_DELAY)
  else:
    print("Connection failed. Exception traceback printed above.")
